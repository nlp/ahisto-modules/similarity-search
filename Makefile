.PHONY: all

HOSTNAME=$(shell hostname)

all: precomputed_result_lists.json cache/precomputed_result_lists.sh-venv-$(HOSTNAME)/

cache/%.sh-venv-$(HOSTNAME)/: requirements/%.txt
	bash create_virtualenv.sh $(notdir $(basename $<).sh)

%.json: %.sh scripts/%.py cache/%.sh-venv-$(HOSTNAME)/
	bash $<
