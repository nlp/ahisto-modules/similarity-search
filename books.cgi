#!/usr/bin/python

from collections import defaultdict
import json
import os
import sys


def get_results(query,
                result_map_filename='precomputed_result_lists.json',
                result_dirname='precomputed_result_lists_data'):
    with open(result_map_filename, 'rt') as f:
        result_map = json.load(f)
    if query not in result_map:
        return '{"error": "nothing found"}'

    result_filename = os.path.join(result_dirname, result_map[query])
    with open(result_filename, 'rt') as f:
        raw_results = json.load(f)[1:]

    result_dict = defaultdict(lambda: (0.0, 0))
    for result in raw_results:
        current_score, current_hits = result_dict[result['id']]
        result_dict[result['id']] = (current_score + result['score'], current_hits + 1)

    results = [
        {
            'hits': num_hits,
            'score': float(num_hits),
            'id': book_id,
        }
        for book_id, (_, num_hits)
        in sorted(result_dict.items(), key=lambda x: (-x[1][0], -x[1][1], x[0]))
    ]
    return json.dumps(results, sort_keys=True, indent=4)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        query = sys.argv[1]
    else:
        print('Content-Type: application/json; charset=utf-8')
        print('Status: 200\n')
        query = os.getenv('PATH_INFO', '/')[1:].decode('utf-8')

    print(get_results(query))
