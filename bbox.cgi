#!/usr/bin/python

import cgi
import json
import os
import sys


def get_results(query, book_id,
                result_map_filename='precomputed_result_lists.json',
                result_dirname='precomputed_result_lists_data'):
    if book_id is None:
        return '{"error": "docid must be specified"}'

    with open(result_map_filename, 'rt') as f:
        result_map = json.load(f)
    if query not in result_map:
        return '{"error": "nothing found"}'

    result_filename = os.path.join(result_dirname, result_map[query])
    with open(result_filename, 'rt') as f:
        raw_results = json.load(f)[1:]
    raw_results = list(filter(lambda result: result['id'] == book_id, raw_results))
    if not raw_results:
        return '{"error": "nothing found"}'

    results = [
        {
            'hits': [
                {
                    'left': result['left_context'],
                    'word': result['center_word'],
                    'right': result['right_context'],
                    'page.n': result['page'],
                    'score': result['score'],
                    'x': 0,
                    'y': 0,
                    'w': 0,
                    'h': 0,
                }
                for result
                in raw_results
            ],
            'id': book_id,
        }
    ]
    return json.dumps(results, sort_keys=True, indent=4)


if __name__ == '__main__':
    if len(sys.argv) > 1:
        query = sys.argv[1]
        book_id = None
        if len(sys.argv) > 2:
            book_id = sys.argv[2]
    else:
        print('Content-Type: application/json; charset=utf-8')
        print('Status: 200\n')
        query = os.getenv('PATH_INFO', '/')[1:].decode('utf-8')
        book_id = cgi.FieldStorage().getfirst('docid', None)

    print(get_results(query, book_id))
