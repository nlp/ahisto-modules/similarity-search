#!/bin/bash
set -e

VIRTUALENV=cache/"$0"-venv-"$(hostname)"
source "$VIRTUALENV"/bin/activate

set -o xtrace

flake8 scripts
pytype scripts
nice -n 10 python -m scripts."${0%.*}"
