from functools import total_ordering
from pathlib import Path
from typing import Set

from IPython.display import display, Markdown
from lxml import etree
from tqdm.autonotebook import tqdm


@total_ordering
class Regest:
    ROOT_PATH = Path('/nlp/projekty/ahisto/public_html/dokumenty/')

    def __init__(self, filename: str):
        self.filename = filename
        self.basename = str(Path(filename).with_suffix(''))
        self.text_filename = self.ROOT_PATH / Path(self.filename).with_suffix('.xml')
        entities: Set['Entity'] = set()
        parser = etree.XMLParser(huge_tree=True, encoding='utf-8')
        with self.text_filename.open('rb') as f:
            document = etree.parse(f, parser)
        for entity in document.xpath('//placeName'):
            entities.add(Place(entity.attrib['reg']))
        for entity in document.xpath('//persName'):
            entities.add(Person(entity.attrib['reg']))
        self.entities = entities

    def __hash__(self):
        return hash(self.basename)

    def __eq__(self, other) -> bool:
        if isinstance(other, Regest):
            return self.basename == other.basename
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, Regest):
            return self.basename < other.basename
        return NotImplemented

    def __str__(self) -> str:
        summary = 'Regest {} with {} entities'.format(self.basename, len(self.entities))
        return summary

    def __repr__(self) -> str:
        return str(self)

    def _ipython_display_(self) -> None:
        summary = 'Regest `{}` with {} entities'.format(self.basename, len(self.entities))
        display(Markdown(summary))


class Entity:
    def __init__(self, text: str):
        self.text = text

    def __hash__(self):
        return hash(self.text)

    def __eq__(self, other) -> bool:
        if isinstance(other, Entity):
            return self.text == other.text
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, Entity):
            return self.text < other.text
        return NotImplemented

    def __str__(self) -> str:
        return self.text

    def __repr__(self) -> str:
        return str(self)

    def _ipython_display_(self) -> None:
        display(Markdown(str(self)))


class Place(Entity):
    def __init__(self, text):
        super().__init__(text)


class Person(Entity):
    def __init__(self, text):
        super().__init__(text)


def entity_factory(filename: str) -> Set[Entity]:
    filename = str(Path(filename).relative_to(Regest.ROOT_PATH))
    regest = Regest(filename)
    return regest.entities


def load_entities() -> Set[Entity]:
    filenames = sorted(Regest.ROOT_PATH.glob('*.xml'))
    all_entities = set()
    for entities in map(entity_factory, tqdm(filenames, desc='Loading entities')):
        all_entities |= entities
    return all_entities
