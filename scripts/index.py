from itertools import chain, islice
from typing import Dict, List, Iterable, Tuple, Optional
from multiprocessing import Pool, set_start_method
from concurrent.futures import ProcessPoolExecutor, as_completed
from pathlib import Path

# from gensim.similarities.annoy import AnnoyIndexer, WordEmbeddingSimilarityIndex
from gensim.similarities import SoftCosineSimilarity, LevenshteinSimilarityIndex, SparseTermSimilarityMatrix
from gensim.corpora import Dictionary
from gensim.utils import simple_preprocess as preprocess
from gensim.models import TfidfModel
from more_itertools import chunked, zip_equal, windowed
import numpy as np

from .document import Document
# from .fasttext import FastText
from tqdm.autonotebook import tqdm


# Default context on some OSs ("spawn") does not allow to access the shared objects
set_start_method('fork')


DICTIONARY: Optional[Dictionary] = None
INDEX: Optional['Index'] = None


def get_document_text(document: Document) -> List[str]:
    return preprocess(document.text)


def get_document_texts(corpus: List[Document], desc: str) -> Iterable[List[str]]:
    with Pool(None) as pool:
        texts = pool.imap(get_document_text, corpus)
        for text in tqdm(texts, total=len(corpus), desc=desc):
            yield text


def doc2bow(document: List[str]) -> List[Tuple[int, int]]:
    return DICTIONARY.doc2bow(document)


def get_results(queries: List[str], topn: int = 1000) -> Tuple[List[str], List[List[Tuple[Document, float]]]]:
    global DICTIONARY
    DICTIONARY = INDEX.dictionary
    # Default context on some OSs ("spawn") does not allow to access the shared objects (DICTIONARY)
    with Pool(None) as pool:
        bow_queries = list(pool.imap(doc2bow, pool.imap(preprocess, queries)))
    DICTIONARY = None
    tfidf_queries = INDEX.query_tfidf[bow_queries]

    with np.errstate(divide='ignore', invalid='ignore'):
        similarities = INDEX.index[tfidf_queries]

    results = [
        [
            (INDEX.corpus[document_number], similarities[query_number][document_number])
            for document_number
            in document_numbers
        ]
        for query_number, document_numbers
        in enumerate(similarities.argsort(axis=1)[:, ::-1][:, :topn])
    ]
    return (queries, results)


def produce_snippet(args: Tuple[str, Document]) -> Tuple[Optional[str], Optional[str], Optional[str]]:
    query, document = args
    return INDEX.produce_snippet(query, document)


class Index:
    BASE_PATH = Path('cache')

    def __init__(self, documents: Dict[str, Document], normalization: Tuple[str, str] = ('maintain', 'maintain')):
        self.normalization = normalization

        self.corpus = sorted(documents.values())
        self.document_numbers = {
            document: document_number
            for document_number, document
            in enumerate(self.corpus)
        }

        dictionary_filename = str(self.BASE_PATH / 'dictionary')
        try:
            print('Loading {}'.format(dictionary_filename))
            self.dictionary = Dictionary.load(dictionary_filename)
        except IOError:
            print('Creating {}'.format(dictionary_filename))
            self.dictionary = Dictionary(get_document_texts(self.corpus, 'Loading document texts'))
            self.dictionary.save(dictionary_filename)

        self.query_tfidf = TfidfModel(dictionary=self.dictionary, smartirs='ltb', slope=0.2)
        self.document_tfidf = TfidfModel(dictionary=self.dictionary, smartirs='lnb', slope=0.2)

        index_filename = str(self.BASE_PATH / 'SCM-index')
        try:
            print('Loading {}'.format(index_filename))
            self.index = SoftCosineSimilarity.load(index_filename)
        except IOError:
            global DICTIONARY
            DICTIONARY = self.dictionary
            # Default context on some OSs ("spawn") does not allow to access the shared objects (DICTIONARY)
            with Pool(None) as pool:
                bow_corpus = list(pool.imap(doc2bow, get_document_texts(self.corpus, 'Creating BoW corpus')))
            DICTIONARY = None
            tfidf_corpus = self.document_tfidf[bow_corpus]

#            we_similarity_matrix_filename = str(self.BASE_PATH / 'SCM-WE-pretrained_similarity-matrix')
#            try:
#                print('Loading {}'.format(we_similarity_matrix_filename))
#                we_similarity_matrix = SparseTermSimilarityMatrix.load(we_similarity_matrix_filename)
#            except IOError:
#                print('Creating {}'.format(we_similarity_matrix_filename))
#                word_embeddings = FastText().wv
#                indexer = AnnoyIndexer(word_embeddings, num_trees=1)
#                we_word_similarities = WordEmbeddingSimilarityIndex(word_embeddings, threshold=-1.0, exponent=4.0, kwargs={'indexer': indexer})
#                we_similarity_matrix = SparseTermSimilarityMatrix(we_word_similarities, self.dictionary, self.document_tfidf, symmetric=True, dominant=True, nonzero_limit=100)
#                we_similarity_matrix.save(we_similarity_matrix_filename)

            ld_similarity_matrix_filename = str(self.BASE_PATH / 'SCM-LD_similarity-matrix')
            try:
                print('Loading {}'.format(ld_similarity_matrix_filename))
                ld_similarity_matrix = SparseTermSimilarityMatrix.load(ld_similarity_matrix_filename)
            except IOError:
                ld_word_similarities_filename = str(self.BASE_PATH / 'SCM-LD_similarity-index')
                try:
                    print('Loading {}'.format(ld_word_similarities_filename))
                    ld_word_similarities = LevenshteinSimilarityIndex.load(ld_word_similarities_filename)
                except IOError:
                    print('Creating {}'.format(ld_word_similarities_filename))
                    ld_word_similarities = LevenshteinSimilarityIndex(self.dictionary, alpha=1.8, beta=5.0, max_distance=3)
                    ld_word_similarities.save(ld_word_similarities_filename)
                print('Creating {}'.format(ld_similarity_matrix_filename))
                ld_similarity_matrix = SparseTermSimilarityMatrix(ld_word_similarities, self.dictionary, self.document_tfidf, symmetric=True, dominant=False, nonzero_limit=100)
                ld_similarity_matrix.save(ld_similarity_matrix_filename)
            # combined_similarity_matrix = SparseTermSimilarityMatrix((0.1 * we_similarity_matrix.matrix + 0.9 * ld_similarity_matrix.matrix))
            combined_similarity_matrix = ld_similarity_matrix

            print('Creating {}'.format(index_filename))
            self.index = SoftCosineSimilarity(tfidf_corpus, combined_similarity_matrix, normalized=self.normalization)
            self.index.save(index_filename)

    def _get_most_important_document_word(self, query: str, document: Document) -> Optional[str]:
        global DICTIONARY
        DICTIONARY = self.dictionary
        bow_query = doc2bow(preprocess(query))
        DICTIONARY = None
        tfidf_query = self.query_tfidf[bow_query]

        document_number = self.document_numbers[document]
        tfidf_document = self.index.corpus[document_number]

        most_important_document_word, maximum_document_word_importance = None, float('-inf')
        for document_word_id, document_word_weight in tfidf_document:
            document_word = self.dictionary.id2token[document_word_id]
            document_word_importance = 0.0
            for query_word_id, query_word_weight in tfidf_query:
                word_similarity = self.index.similarity_matrix.matrix[query_word_id, document_word_id]
                word_pair_importance = query_word_weight * word_similarity * document_word_weight
                document_word_importance += word_pair_importance
            if document_word_importance > maximum_document_word_importance:
                most_important_document_word = document_word
                maximum_document_word_importance = document_word_importance

        return most_important_document_word

    def _get_most_important_document_context(self, query: str, document: Document, center_word: str, window_size: int = 10) -> Tuple[Optional[str], Optional[str]]:
        if center_word is None:
            return None, None

        global DICTIONARY
        DICTIONARY = self.dictionary
        bow_query = doc2bow(preprocess(query))
        DICTIONARY = None
        tfidf_query = self.query_tfidf[bow_query]

        padding: Iterable[Optional[str]] = (window_size - 1) * [None]
        document_text: Iterable[Optional[str]] = get_document_text(document)
        padded_document_text = chain(padding, document_text, padding)
        windows = windowed(padded_document_text, 2 * window_size + 1)
        most_important_left_context, most_important_right_context = None, None
        maximum_context_importance = float('-inf')
        for window in windows:
            current_center_word = window[window_size]
            if current_center_word != center_word:
                continue

            left_context = list(filter(lambda word: word is not None, window[:window_size]))
            right_context = list(filter(lambda word: word is not None, window[window_size + 1:]))
            context = list(chain(left_context, [center_word], right_context))
            DICTIONARY = self.dictionary
            tfidf_context = self.document_tfidf[doc2bow(context)]
            DICTIONARY = None
            context_importance = self.index.similarity_matrix.inner_product(tfidf_query, tfidf_context, normalized=self.normalization)
            if context_importance > maximum_context_importance:
                most_important_left_context = ' '.join(left_context)
                most_important_right_context = ' '.join(right_context)
                maximum_context_importance = context_importance

        return most_important_left_context, most_important_right_context

    def produce_snippet(self, query: str, document: Document) -> Tuple[Optional[str], Optional[str], Optional[str]]:
        center_word = self._get_most_important_document_word(query, document)
        left_context, right_context = self._get_most_important_document_context(query, document, center_word)
        return left_context, center_word, right_context

    def bulk_query(self, queries: Iterable[str], chunk_size: int = 1000) -> Iterable[Tuple[str, Iterable[Tuple[Tuple[Document, float], Tuple[str, str, str]]]]]:
        global INDEX
        INDEX = self
        with ProcessPoolExecutor(1) as executor:
            queries = list(queries)
            progress_bar = iter(tqdm(queries, desc='Querying the index'))
            result_chunk_futures = [executor.submit(get_results, query_chunk) for query_chunk in chunked(queries, chunk_size)]

            for result_chunk_future in as_completed(result_chunk_futures):
                query_chunk, result_chunk = result_chunk_future.result()
                flat_query_and_document_chunk = chain(*[
                    (
                        (query, document)
                        for document, _
                        in results
                    )
                    for query, results
                    in zip_equal(query_chunk, result_chunk)
                ])
                # Default context on some OSs ("spawn") does not allow to access the shared objects (INDEX)
                with Pool(None) as pool:
                    flat_snippet_chunk = pool.imap(produce_snippet, flat_query_and_document_chunk)
                    snippet_chunk = (islice(flat_snippet_chunk, len(results)) for results in result_chunk)
                    for query, results, snippets in zip_equal(query_chunk, result_chunk, snippet_chunk):
                        yield query, zip_equal(results, snippets)
                        next(progress_bar)
        INDEX = None
