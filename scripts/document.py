from functools import total_ordering
from pathlib import Path
from typing import Dict, Tuple

from IPython.display import display, HTML, Markdown
from tqdm.autonotebook import tqdm


@total_ordering
class Document:
    ROOT_PATH = Path('/nlp/projekty/ahisto/public_html/ocr-output/')
    SNIPPET_SIZE = 100

    def __init__(self, filename: str):
        self.filename = filename
        self.basename = str(Path(filename).with_suffix(''))
        self.book_id, self.page_id = map(int, self.basename.split('/'))
        self.document_url = f'https://nlp.fi.muni.cz/projekty/ahisto/portal/book.php?book={self.book_id}&page={self.page_id}'
        self.image_url = f'https://nlp.fi.muni.cz/projekty/ahisto/book_by_id/{self.book_id}/{self.page_id}.jpg'
        self.text_filename = self.ROOT_PATH / Path(self.filename).with_suffix('.txt')

    @property
    def text(self) -> str:
        with self.text_filename.open('rt') as f:
            return f.read()

    def __hash__(self):
        return hash(self.basename)

    def __eq__(self, other) -> bool:
        if isinstance(other, Document):
            return self.basename == other.basename
        return NotImplemented

    def __lt__(self, other) -> bool:
        if isinstance(other, Document):
            return self.basename < other.basename
        return NotImplemented

    def __str__(self) -> str:
        summary = ' '.join(str(self.text).split())
        if len(summary) > self.SNIPPET_SIZE:
            summary = '{}...'.format(summary[:self.SNIPPET_SIZE])
        return summary

    def __repr__(self) -> str:
        return str(self)

    def _ipython_display_(self) -> None:
        display(
            Markdown(f'**[Document {self.basename}]({self.document_url}):** {str(self)}'),
            HTML(f'<img src="{self.image_url}" width="500" alt="Document {self.basename}" />'),
        )


def document_factory(filename: str) -> Tuple[str, Document]:
    filename = str(Path(filename).relative_to(Document.ROOT_PATH))
    document = Document(filename)
    return (document.basename, document)


def load_documents() -> Dict[str, Document]:
    filenames = sorted(Document.ROOT_PATH.glob('*/*.txt'))
    documents = {
        basename: document
        for basename, document
        in map(document_factory, tqdm(filenames, desc='Loading documents'))
    }
    return documents
