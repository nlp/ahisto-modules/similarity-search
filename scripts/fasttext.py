from pathlib import Path
from urllib.request import urlretrieve

from gensim.models.keyedvectors import KeyedVectors


class FastText:
    BASE_PATH = Path('cache')

    def __init__(self, lang: str = 'cs'):
        path = self.BASE_PATH / f'cc.{lang}.300.vec.gz'
        if not path.exists():
            print(f'Downloading fastText embeddings for language {lang}')
            url = f'https://dl.fbaipublicfiles.com/fasttext/vectors-crawl/cc.{lang}.300.vec.gz'
            urlretrieve(url, path)
        print(f'Loading fastText embeddings for language {lang}')
        self.wv = KeyedVectors.load_word2vec_format(path)
