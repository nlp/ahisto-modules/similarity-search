import json
from pathlib import Path
import sys

from .document import load_documents
from .entity import load_entities
from .index import Index


if __name__ == '__main__':
    entities = load_entities()
    # Include an example query in the results
    queries = list(map(str, sorted(entities))) + ['Jan Žižka píše svým stoupencům v Skalici']
    documents = load_documents()
    index = Index(documents)

    query_results = index.bulk_query(queries)

    query_to_results = dict()
    query_to_results_filename = Path(Path(sys.argv[0]).with_suffix('.json').name)
    output_dirname = Path('{}_data'.format(query_to_results_filename.with_suffix('')))
    output_dirname.mkdir(parents=False, exist_ok=True)
    for query_number, (query, results_and_snippets) in enumerate(query_results):
        query_filename = '{:05d}.json'.format(query_number + 1)
        with (output_dirname / query_filename).open('wt') as f:
            # Include query in case we need to reconstruct query_to_results from partial data
            json.dump([query] + [
                {
                    'id': str(document.book_id),
                    'page': str(document.page_id),
                    'score': float(score),
                    'left_context': left_context if left_context is not None else '',
                    'center_word': center_word if center_word is not None else '',
                    'right_context': right_context if right_context is not None else '',
                }
                for (document, score), (left_context, center_word, right_context)
                in results_and_snippets
            ], f, sort_keys=True, indent=4)
        query_to_results[query] = query_filename
    with query_to_results_filename.open('wt') as f:
        json.dump(query_to_results, f, sort_keys=True, indent=4)
