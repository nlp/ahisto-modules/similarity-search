#!/bin/bash
set -e -o xtrace

VIRTUALENV=cache/"$1"-venv-"$(hostname)"
rm -rf "$VIRTUALENV"
trap 'rm -rf "$VIRTUALENV"' EXIT
python3.7 -m venv "$VIRTUALENV"
source "$VIRTUALENV"/bin/activate
pip install -U pip
pip install -r requirements/"${1%.*}".txt
trap '' EXIT
